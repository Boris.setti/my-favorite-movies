# Readme GPS For Volt

## Prérequis
1. Avoir installé React-native `npm install -g react-native-cli`.
2. Installer un émulateur android ou avoir un smartphone android.
  * si émulateur android dans la doc [React Native](https://facebook.github.io/react-native/docs/getting-started) suivre le tuto React Native CLI Quickstart.
  * si smartphone android dans la doc [React Native](https://facebook.github.io/react-native/docs/running-on-device) suivre le tuto pour android.
  * passer votre smartphone en mode développeur. 
  
  ## Utilisation 
  1. Dans le dossier android crée un fichier `local.properties` et écrire `sdk.dir = /home/__USER NAME__/Android/Sdk`
  2. `npm install`
  3. `react-native run-android`
  4. `npm start`
