const API_TOKEN = 'f86534d6f9529e053cad2ebd8c786d32'

export function getFilmsFromApiWithSearchedText (text,page ) {
    const url = 'https://api.themoviedb.org/3/search/movie?api_key=' + API_TOKEN + '&language=fr&query=' + text  + "&page=" + page
    return fetch(url)  //verify la requête API
    .then((response) => response.json()) // si OK
    .catch((error) => console.error(error)) //si problème
  }

export function getImageFromApi (name) {
  return 'https://image.tmdb.org/t/p/w300' + name
}

export function getFilmDetailFromApi(id){
  const url = 'https://api.themoviedb.org/3/movie/' + id + '?api_key=' + API_TOKEN + '&language=fr'
  return fetch(url)
  .then((response) => response.json())
  .catch((error) => console.error(error));
}